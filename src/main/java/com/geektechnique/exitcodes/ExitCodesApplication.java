package com.geektechnique.exitcodes;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeExceptionMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ExitCodesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExitCodesApplication.class, args);
    }

    public int getExitCode() {
        return 42;
    }

    @Bean
    CommandLineRunner createException() {
        return args -> Integer.parseInt("test") ;
    }

    @Bean
    ExitCodeExceptionMapper exitCodeToexceptionMapper() {
        return exception -> {
            // set exit code base on the exception type
            if (exception.getCause() instanceof NumberFormatException) {
                return 80;
            }
            return 1;
        };
    }

    @Bean
    DemoListener demoListenerBean() {
        return new DemoListener();
    }

}
