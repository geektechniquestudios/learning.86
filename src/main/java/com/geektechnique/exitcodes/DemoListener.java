package com.geektechnique.exitcodes;

import org.springframework.boot.ExitCodeEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

public class DemoListener {
    @EventListener
    public void exitEvent(ExitCodeEvent event) {
        System.out.println("Exit code: " + event.getExitCode());
    }
}

